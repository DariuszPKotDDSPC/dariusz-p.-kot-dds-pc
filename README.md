At the office of Dariusz P. Kot, DDS, PC, we are committed to ensuring your dental experience is affordable, comfortable, and personalized. Dr. Kot and our team have served the Fairfax, VA community for over 20 years, and always welcome new patients into our practice family.

We pride ourselves on providing a comprehensive approach to your dental health. Our services include general and restorative dentistry, cosmetic dentistry, periodontal care, sleep apnea and snoring prevention, teeth whitening, and InvisalignÂ® clear orthodontic aligners.

Address: 11211 Waples Mill Rd, Suite 300, Fairfax, VA 22030

Phone: 703-352-8015
